package niaz.kinopoisk2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by NS on 02.11.2016.
 */
public class City {
    private static final String	JSON_PROPERTY_CITY_ID	= "cityID";
    private static final String	JSON_PROPERTY_CITY_NAME	= "cityName";

    @JsonIgnore
    private String cityID = null;

    @JsonIgnore
    private String cityName = null;

    /**
     * Constructor
     */
    public City(){
    }

    // Setters, getters

    @JsonProperty(JSON_PROPERTY_CITY_ID)
    public void setCityID(String cityID){
        this.cityID = cityID;
    }

    @JsonProperty(JSON_PROPERTY_CITY_ID)
    public String getCityID(){
        return cityID;
    }

    @JsonProperty(JSON_PROPERTY_CITY_NAME)
    public void setCityName(String cityName){
        this.cityName = cityName;
    }

    @JsonProperty(JSON_PROPERTY_CITY_NAME)
    public String getCityName(){
        return cityName;
    }
}
