package niaz.kinopoisk2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by NS on 02.11.2016.
 */
public class Cities {
    private static final String	JSON_PROPERTY_COUNTRY_ID	= "countryID";
    private static final String	JSON_PROPERTY_COUNTRY_NAME	= "countryName";
    private static final String	JSON_PROPERTY_CITY_DATA	    = "cityData";

    @JsonIgnore
    private String countryID = null;

    @JsonIgnore
    private String countryName = null;

    @JsonIgnore
    private ArrayList<City> cityData = new ArrayList<City>();

    /**
     * Constructor
     */
    public Cities(){
    }

    @JsonProperty(JSON_PROPERTY_COUNTRY_ID)
    public void setCountryID(String countryID){
        this.countryID = countryID;
    }

    @JsonProperty(JSON_PROPERTY_COUNTRY_ID)
    public String getCountryID(){
        return countryID;
    }

    @JsonProperty(JSON_PROPERTY_COUNTRY_NAME)
    public void setCountryName(String countryName){
        this.countryName = countryName;
    }

    @JsonProperty(JSON_PROPERTY_COUNTRY_NAME)
    public String getCountryName(){
        return countryName;
    }

    @JsonProperty(JSON_PROPERTY_CITY_DATA)
    public ArrayList<City> getCityData(){
        return cityData;
    }
}
