package niaz.kinopoisk2.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by NS on 02.11.2016.
 */
public class MyMovies {
    private static final String	JSON_PROPERTY_FILMS_DATA	        = "filmsData";
    private static final String	JSON_PROPERTY_DATE	        = "date";

    private ArrayList<MyMovie> filmsData = new ArrayList<>();
    private String date = null;


    /**
     * Constructor for Jackson
     */
    public MyMovies(){

    }

    // Setters, getters
    @JsonProperty(JSON_PROPERTY_FILMS_DATA)
    public void setFilmsData(ArrayList<MyMovie> filmsData){
        this.filmsData = filmsData;
    }

    @JsonProperty(JSON_PROPERTY_FILMS_DATA)
    public ArrayList<MyMovie> getFilmsData(){
        return filmsData;
    }

    @JsonProperty(JSON_PROPERTY_DATE)
    public void setDate(String date){
        this.date = date;
    }

    @JsonProperty(JSON_PROPERTY_DATE)
    public String getDate(){
        return date;
    }
}
