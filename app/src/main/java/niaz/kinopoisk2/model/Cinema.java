package niaz.kinopoisk2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by NS on 05.11.2016.
 */
public class Cinema {
    private static final String	JSON_PROPERTY_ADDRESS   = "address";
    private static final String	JSON_PROPERTY_ID	    = "cinemaID";
    private static final String	JSON_PROPERTY_NAME	    = "cinemaName";
    private static final String	JSON_PROPERTY_TYPE	    = "type";
    private static final String	JSON_PROPERTY_LON	    = "lon";
    private static final String	JSON_PROPERTY_LAT	    = "lat";
    private static final String	JSON_PROPERTY_NO_SEANCES= "hasNoSeances";

    @JsonIgnore
    private String address  = null;

    @JsonIgnore
    private String id       = null;

    @JsonIgnore
    private String name     = null;

    @JsonIgnore
    private String type     = null;

    @JsonIgnore
    private String lon      = null;

    @JsonIgnore
    private String lat      = null;

    @JsonIgnore
    private String hasNoSeances = null;

    /**
     * Constructor for JSON Jackson
     */
    public Cinema(){

    }

    //Setters, getters
    @JsonProperty(JSON_PROPERTY_ADDRESS)
    public void setAddress(String address){
        this.address = address;
    }

    @JsonProperty(JSON_PROPERTY_ADDRESS)
    public String getAddress(){
        return address;
    }

    @JsonProperty(JSON_PROPERTY_ID)
    public void setId(String id){
        this.id = id;
    }

    @JsonProperty(JSON_PROPERTY_ID)
    public String getId(){
        return id;
    }

    @JsonProperty(JSON_PROPERTY_NAME)
    public void setName(String name){
        this.name = name;
    }

    @JsonProperty(JSON_PROPERTY_NAME)
    public String getName(){
        return name;
    }

    @JsonProperty(JSON_PROPERTY_TYPE)
    public void set(String type){
        this.type = type;
    }

    @JsonProperty(JSON_PROPERTY_TYPE)
    public String getType(){
        return type;
    }

    @JsonProperty(JSON_PROPERTY_LON)
    public void setLon(String lon){
        this.lon = lon;
    }

    @JsonProperty(JSON_PROPERTY_LON)
    public String getLon(){
        return lon;
    }

    @JsonProperty(JSON_PROPERTY_LAT)
    public void setLat(String lat){
        this.lat = lat;
    }

    @JsonProperty(JSON_PROPERTY_LAT)
    public String getLat(){
        return lat;
    }

    @JsonProperty(JSON_PROPERTY_NO_SEANCES)
    public void setHasNoSeances(String hasNoSeances){
        this.hasNoSeances = hasNoSeances;
    }

    @JsonProperty(JSON_PROPERTY_NO_SEANCES)
    public String getHasNoSeances(){
        return hasNoSeances;
    }

}
