package niaz.kinopoisk2.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by NS on 05.11.2016.
 */
public class Cinemas {
    private static final String	JSON_PROPERTY_CINEMAS   = "searchCinemas";
    private static final String	JSON_PROPERTY_COUNT	    = "searchCinemasCountResult";
    private static final String	JSON_PROPERTY_PAGES     = "pagesCount";
    private static final String	JSON_PROPERTY_KEYWORD   = "keyword";

    private ArrayList<Cinema>  cinemaList          = null;
    private int     searchCinemasCountResult    = 0;
    private int     pagesCount                  = 0;
    private String  keyword                     = null;

    /**
     * Constructor for JSON
     */
    public Cinemas() {

    }

    //Setters, getters
    @JsonProperty(JSON_PROPERTY_CINEMAS)
    public void setCinemaList(ArrayList<Cinema> cinemaList) {
        this.cinemaList = cinemaList;
    }

    @JsonProperty(JSON_PROPERTY_CINEMAS)
    public ArrayList<Cinema> getCinemaList(){
        return cinemaList;
    }

    @JsonProperty(JSON_PROPERTY_COUNT)
    public void setSearchCinemasCountResult(int searchCinemasCountResult){
        this.searchCinemasCountResult = searchCinemasCountResult;
    }

    @JsonProperty(JSON_PROPERTY_COUNT)
    public int getSearchCinemasCountResult(){
        return searchCinemasCountResult;
    }

    @JsonProperty(JSON_PROPERTY_PAGES)
    public void setPagesCount(int pagesCount){
        this.pagesCount = pagesCount;
    }

    @JsonProperty(JSON_PROPERTY_PAGES)
    public int getPagesCount(){
        return pagesCount;
    }

    @JsonProperty(JSON_PROPERTY_KEYWORD)
    public void setKeyword(String keyword){
        this.keyword = keyword;
    }

    @JsonProperty(JSON_PROPERTY_KEYWORD)
    public String getKeyword(){
        return keyword;
    }
}
