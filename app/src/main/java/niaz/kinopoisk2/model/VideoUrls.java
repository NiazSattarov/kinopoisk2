package niaz.kinopoisk2.model;
        import com.fasterxml.jackson.annotation.JsonProperty;

        import java.util.ArrayList;

/**
 * Created by NS on 02.11.2016.
 */
public class VideoUrls {
    private static final String	JSON_PROPERTY_HD    = "hd";
    private static final String	JSON_PROPERTY_SD    = "sd";
    private static final String	JSON_PROPERTY_LOW   = "low";

    private String hdUrl = null;
    private String sdUrl = null;
    private String lowUrl = null;

    /**
     * Constructor for Jackson
     */
    public VideoUrls(){

    }

    // Setters, getters

    @JsonProperty(JSON_PROPERTY_HD)
    public void setHdUrl(String hdUrl){
        this.hdUrl = hdUrl;
    }

    @JsonProperty(JSON_PROPERTY_HD)
    public String getHdUrl(){
        return hdUrl;
    }

    @JsonProperty(JSON_PROPERTY_SD)
    public void setSdUrl(String sdUrl){
        this.sdUrl = sdUrl;
    }

    @JsonProperty(JSON_PROPERTY_SD)
    public String getSdUrl(){
        return sdUrl;
    }

    @JsonProperty(JSON_PROPERTY_LOW)
    public void setLowUrl(String lowUrl){
        this.lowUrl = lowUrl;
    }

    @JsonProperty(JSON_PROPERTY_LOW)
    public String getLowUrl(){
        return lowUrl;
    }

}
