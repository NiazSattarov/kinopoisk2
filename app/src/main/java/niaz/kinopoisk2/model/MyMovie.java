package niaz.kinopoisk2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by NS on 02.11.2016.
 */
public class MyMovie {
    private static final String	JSON_PROPERTY_TYPE	        = "type";
    private static final String	JSON_PROPERTY_ID	        = "id";
    private static final String	JSON_PROPERTY_NAME_RU	    = "nameRU";
    private static final String	JSON_PROPERTY_NAME_EN	    = "nameEN";
    private static final String	JSON_PROPERTY_YEAR	        = "year";
    private static final String	JSON_PROPERTY_CINEMA	    = "cinemaHallCount";
    private static final String	JSON_PROPERTY_3D	        = "is3D";
    private static final String	JSON_PROPERTY_IMAX	        = "isIMAX";
    private static final String	JSON_PROPERTY_NEW	        = "isNew";
    private static final String	JSON_PROPERTY_RATING	    = "rating";
    private static final String	JSON_PROPERTY_POSTER	    = "posterURL";
    private static final String	JSON_PROPERTY_FILM_LENGTH	= "filmLength";
    private static final String	JSON_PROPERTY_COUNTRY	    = "country";
    private static final String	JSON_PROPERTY_GENRE	        = "genre";
    private static final String	JSON_PROPERTY_PREMIERE	    = "premiereRU";
    private static final String	JSON_PROPERTY_VIDEO	        = "videoURL";

    /**
     * Constructor for Jackson
     */
    public MyMovie(){

    }

    @JsonIgnore
    private String type = "";

    @JsonIgnore
    private String id = "";

    @JsonIgnore
    private String nameRU = "";

    @JsonIgnore
    private String nameEN = "";

    @JsonIgnore
    private String year = "";

    @JsonIgnore
    private String cinemaHallCount = "";

    @JsonIgnore
    private String feature3D = "";

    @JsonIgnore
    private String featureIMAX = "";

    @JsonIgnore
    private String featureNew = "";

    @JsonIgnore
    private String rating = "";

    @JsonIgnore
    private String posterURL = "";

    @JsonIgnore
    private String filmLength = "";

    @JsonIgnore
    private String country = "";

    @JsonIgnore
    private String genre = "";

    @JsonIgnore
    private String premiereRU = "";

    @JsonIgnore
    private VideoUrls videoUrls = null;

    //Setters, getters

    @JsonProperty(JSON_PROPERTY_TYPE)
    public void setType(String type){
        this.type = type;
    }

    @JsonProperty(JSON_PROPERTY_TYPE)
    public String getType(){
        return type;
    }


    @JsonProperty(JSON_PROPERTY_ID)
    public void setId(String id){
        this.id = id;
    }

    @JsonProperty(JSON_PROPERTY_ID)
    public String getId(){
        return id;
    }

    @JsonProperty(JSON_PROPERTY_NAME_RU)
    public void setNameRU(String nameRU){
        this.nameRU = nameRU;
    }

    @JsonProperty(JSON_PROPERTY_NAME_RU)
    public String getNameRU(){
        return nameRU;
    }

    @JsonProperty(JSON_PROPERTY_NAME_EN)
    public void setNameEN(String nameEN){
        this.nameEN = nameEN;
    }

    @JsonProperty(JSON_PROPERTY_NAME_EN)
    public String getNameEN(){
        return nameEN;
    }

    @JsonProperty(JSON_PROPERTY_YEAR)
    public void setYear(String year){
        this.year = year;
    }

    @JsonProperty(JSON_PROPERTY_YEAR)
    public String getYear(){
        return year;
    }

    @JsonProperty(JSON_PROPERTY_CINEMA)
    public void setCinemaHallCount(String cinemaHallCount){
        this.cinemaHallCount = cinemaHallCount;
    }

    @JsonProperty(JSON_PROPERTY_CINEMA)
    public String getCinemaHallCount(){
        return cinemaHallCount;
    }

    @JsonProperty(JSON_PROPERTY_3D)
    public void setFeature3D(String feature3D){
        this.feature3D = feature3D;
    }

    @JsonProperty(JSON_PROPERTY_3D)
    public String getFeature3D(){
        return feature3D;
    }

    @JsonProperty(JSON_PROPERTY_IMAX)
    public void setFeatureIMAX(String featureIMAX){
        this.featureIMAX = featureIMAX;
    }

    @JsonProperty(JSON_PROPERTY_IMAX)
    public String getFeatureIMAX(){
        return featureIMAX;
    }

    @JsonProperty(JSON_PROPERTY_NEW)
    public void setFeatureNew(String featureNew){
        this.featureNew = featureNew;
    }

    @JsonProperty(JSON_PROPERTY_NEW)
    public String getFeatureNew(){
        return featureNew;
    }

    @JsonProperty(JSON_PROPERTY_RATING)
    public void setRating(String rating){
        this.rating = rating;
    }

    @JsonProperty(JSON_PROPERTY_RATING)
    public String getRating(){
        return rating;
    }

    @JsonProperty(JSON_PROPERTY_POSTER)
    public void setPosterURL(String posterURL){
        this.posterURL = posterURL;
    }

    @JsonProperty(JSON_PROPERTY_POSTER)
    public String getPosterURL(){
        return posterURL;
    }

    @JsonProperty(JSON_PROPERTY_FILM_LENGTH)
    public void setFilmLength(String filmLength){
        this.filmLength = filmLength;
    }

    @JsonProperty(JSON_PROPERTY_FILM_LENGTH)
    public String getFilmLength(){
        return filmLength;
    }

    @JsonProperty(JSON_PROPERTY_COUNTRY)
    public void setCountry(String country){
        this.country = country;
    }

    @JsonProperty(JSON_PROPERTY_COUNTRY)
    public String getCountry(){
        return country;
    }

    @JsonProperty(JSON_PROPERTY_GENRE)
    public void setGenre(String genre){
        this.genre = genre;
    }

    @JsonProperty(JSON_PROPERTY_GENRE)
    public String getGenre(){
        return genre;
    }

    @JsonProperty(JSON_PROPERTY_PREMIERE)
    public void setPremiereRU(String premiereRU){
        this.premiereRU = premiereRU;
    }

    @JsonProperty(JSON_PROPERTY_PREMIERE)
    public String getPremiereRU(){
        return premiereRU;
    }

    @JsonProperty(JSON_PROPERTY_VIDEO)
    public void setVideoUrls(VideoUrls videoUrls){
        this.videoUrls = videoUrls;
    }

    @JsonProperty(JSON_PROPERTY_VIDEO)
    public VideoUrls getVideoUrls(){
        return videoUrls;
    }
}
