package niaz.kinopoisk2.model;
        import com.fasterxml.jackson.databind.ObjectMapper;

        import java.io.IOException;

/**
 * Created by NS on 01.11.2016.
 */
public class Tools {

    /**
     * Convert JSON to list of cities objects
     * @param str
     * @return
     */
    public static Cities convertJsonToCities (String str)
    {
        Cities cities = null;

        //  Convert JSON to Java object
        ObjectMapper mapper = new ObjectMapper();

        //JSON from String to Object
        try {
            cities = mapper.readValue(str, Cities.class);
        } catch (IOException e) {
            System.out.println("*** Error converting Json to Cities:" + e);
            return null;
        }

        return cities;
    }

    /**
     * Convert JSON to City object
     * @param str
     * @return
     */
    public static MyMovies convertJsonToMoviesInCity (String str)
    {
        MyMovies myMovies = null;

        //  Convert JSON to Java object
        ObjectMapper mapper = new ObjectMapper();

        //JSON from String to Object
        try {
            myMovies = mapper.readValue(str, MyMovies.class);
        } catch (IOException e) {
            System.out.println("*** Error converting Json to Cities:" + e);
            return null;
        }

        return myMovies;
    }

    /**
     * Convert JSON to Cinemas object
     * @param str
     * @return
     */
    public static Cinemas convertJsonToCinemas (String str)
    {
        Cinemas cinemas = null;

        //  Convert JSON to Java object
        ObjectMapper mapper = new ObjectMapper();

        //JSON from String to Object
        try {
            cinemas = mapper.readValue(str, Cinemas.class);
        } catch (Exception e) {
            System.out.println("*** Error converting Json to Cinemas:" + e);
            return null;
        }

        return cinemas;
    }

}
