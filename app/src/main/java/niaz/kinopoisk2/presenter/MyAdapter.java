package niaz.kinopoisk2.presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import niaz.kinopoisk2.R;
import niaz.kinopoisk2.model.DownloadImage;
import niaz.kinopoisk2.model.MyMovie;

/**
 * Created by NS on 02.11.2016.
 */
public class MyAdapter extends ArrayAdapter<MyMovie> {

    private ArrayList<MyMovie>  movies      = new ArrayList<>();
    private ImageView           imageView   = null;
    private Context             context     = null;

    /**
     * Constructor
     * @param context
     * @param textViewResourceId
     * @param objects
     */
    public MyAdapter(Context context, int textViewResourceId, ArrayList<MyMovie> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        movies = objects;
    }

    // Setters, getters
    @Override
    public int getCount() {
        return super.getCount();
    }

    /**
     * Get view. Set values for each row.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.movie, null);

        TextView textViewName = (TextView) v.findViewById(R.id.movie_name);
        textViewName.setText(movies.get(position).getNameRU());

        TextView textViewInfo = (TextView) v.findViewById(R.id.movie_info);
        textViewInfo.setText(movies.get(position).getGenre());

//        TextView textViewDate = (TextView) v.findViewById(R.id.movie_info1);
//        textViewDate.setText(movies.get(position).getGenre());

        imageView = (ImageView) v.findViewById(R.id.movie_photo);
        String url = "https://st.kp.yandex.net/images/film_iphone/iphone360_" +
                movies.get(position).getId() + ".jpg";
        Bitmap bitmap = MyCache.getFromCache(url);

        // Image is not downloaded
        if (bitmap == null) {
            new DownloadImage(imageView).execute(url);
        }
        // Set image from the cache
        else {
            imageView.setImageBitmap(bitmap);
        }

//        Button buttonCinemas = (Button) v.findViewById(R.id.movie_button_map);
//        buttonCinemas.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent= new Intent(context, CityMapActivity.class);
//                context.startActivity(intent);
//            }
//        });

        return v;
    }


}