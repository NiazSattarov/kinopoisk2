package niaz.kinopoisk2.presenter;

import android.graphics.Bitmap;

import java.util.HashMap;

/**
 * Created by NS on 02.11.2016.
 */
public class MyCache {

    /**
     * My memory cache data
     */
    private static HashMap<String,Bitmap> imageCache =
            new HashMap<String,Bitmap>();

    /**
     * Add to cash
     * @param key
     * @param bitmap
     * @return
     */
    public static boolean addToCache(String key, Bitmap bitmap) {

        if (imageCache.containsKey(key)) {
            return false;
        }
        imageCache.put(key, bitmap);
        return true;
    }

    /**
     * Get from my cash
     * @param key
     * @return
     */
    public static Bitmap getFromCache(String key) {
        return imageCache.get(key);
    }
}
