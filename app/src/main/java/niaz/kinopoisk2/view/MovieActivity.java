package niaz.kinopoisk2.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import niaz.kinopoisk2.presenter.MyCache;
import niaz.kinopoisk2.R;
import niaz.kinopoisk2.model.DownloadImage;

/**
 * Created by NS on 03.11.2016.
 */
public class MovieActivity extends Activity {
    private int     cityPosition    = 0;
    private String  cityId          = "";
    private String  cityName        = "";
    private String  movieId         = "";
    private String  movieName       = "";

    /**
     * On Create
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        movieId = getIntent().getStringExtra("id");
        movieName = getIntent().getStringExtra("movieName");
        cityId = getIntent().getStringExtra("cityID");
        cityName = getIntent().getStringExtra("cityName");
        cityPosition = getIntent().getIntExtra("cityPosition", 0);
        System.out.println("=================Movie Info=========city=" + cityId);
        initViews();
    }

    /**
     * Process back key
     */
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MovieActivity.this, MoviesActivity.class);
        intent.putExtra("cityID", cityId);
        intent.putExtra("cityName", cityName);
        intent.putExtra("cityPosition", cityPosition);
        startActivity(intent);
        finish();
    }

    /**
     * Init views
     */
    private void initViews() {
        TextView textView = (TextView) findViewById(R.id.movie_title);
        textView.setText(movieName);
        ImageView imageView = (ImageView) findViewById(R.id.movie_poster);
        Button buttonCinemas = (Button) findViewById(R.id.movie_button_map);
        buttonCinemas.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(MovieActivity.this, CityMapActivity.class);
                intent.putExtra("cityID", cityId);
                intent.putExtra("cityName", cityName);
                intent.putExtra("cityPosition", cityPosition);
                intent.putExtra("id", movieId);
                intent.putExtra("movieName", movieName);
                startActivity(intent);
            }
        });

        String url = "https://st.kp.yandex.net/images/film_iphone/iphone360_" +
                movieId + ".jpg";
        Bitmap bitmap = MyCache.getFromCache(url);

        // Image is not downloaded
        if (bitmap == null) {
            new DownloadImage(imageView).execute(url);
        }
        // Set image from the cache
        else {
            imageView.setImageBitmap(bitmap);
        }
    }
}