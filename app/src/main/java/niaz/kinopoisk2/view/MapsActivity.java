package niaz.kinopoisk2.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import niaz.kinopoisk2.R;
import niaz.kinopoisk2.model.Cities;
import niaz.kinopoisk2.model.City;
import niaz.kinopoisk2.model.Tools;

/**
 * Created by Niaz Sattarov on 2.11.2016
 */
public class MapsActivity extends Activity {

    private Spinner         spinner         = null;
    private ProgressBar     progressBar     = null;
    private List<String>    listCitiesNames = new ArrayList<String>();
    private List<String>    listCitiesIds   = new ArrayList<String>();
    private int             selectedCityPos = 0;

    /**
     * OnCreate
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("===========Start==========");
        setContentView(R.layout.activity_main);
        initViews();
        if (listCitiesNames.size() == 0) {
            downloadCities();
        }
        selectedCityPos = getIntent().getIntExtra("cityPosition", 0);

    }

    /**
     * Create options menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * if item selected in options menu
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Init views
     */
    private void initViews() {
        spinner = (Spinner) findViewById(R.id.page_cities_spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            protected Adapter initializedAdapter = null;

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (initializedAdapter != parentView.getAdapter()) {
                    initializedAdapter = parentView.getAdapter();
                    return;
                }
                selectedCityPos = position;
                System.out.println("+++++++++ selected at position=" + listCitiesNames.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        initSpinnerData();
        progressBar = (ProgressBar) findViewById(R.id.page_cities_progress);
        Button buttonMovies = (Button) findViewById(R.id.page_cities_button_movies);
        buttonMovies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MapsActivity.this, MoviesActivity.class);
                System.out.println("---- MapsActivity cityId=" + listCitiesIds.get(selectedCityPos));
                intent.putExtra("cityID", listCitiesIds.get(selectedCityPos));
                intent.putExtra("cityName", listCitiesNames.get(selectedCityPos));
                intent.putExtra("cityPosition", selectedCityPos);
                startActivity(intent);
                finish();
            }
        });


    }

    /**
     * Download cities from api.kinopoisk.cf
     */
    private void downloadCities(){
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://api.kinopoisk.cf/getCityList?countryID=2", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                progressBar.setVisibility(View.INVISIBLE);
                setSpinnerData(new String(response));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "Error to download cities", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });    }

    /**
     * Process downloaded data and set spinner data
     * @param str
     */
    private void setSpinnerData(String str){
        Cities cities = Tools.convertJsonToCities(str);
        if (cities == null) {
            System.out.println("-------cities == null");
            return;
        }

        ArrayList<City> cityData = cities.getCityData();
        if (cityData == null) {
            System.out.println("-------cityData == null");
            return;
        }

        for (City city : cityData) {
            listCitiesIds.add(city.getCityID());
            listCitiesNames.add(city.getCityName());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, listCitiesNames);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner.setAdapter(dataAdapter);
        spinner.setSelection(selectedCityPos);
    }

    /**
     * Init spinner data
     */
    private void initSpinnerData(){
        ArrayList<String> list = new ArrayList<String>();
        list.add(getResources().getString(R.string.page_cities_city_not_selected));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner.setAdapter(dataAdapter);
    }
}
