package niaz.kinopoisk2.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import java.sql.Date;
import java.util.ArrayList;
import cz.msebera.android.httpclient.Header;
import niaz.kinopoisk2.R;
import niaz.kinopoisk2.model.MyMovies;
import niaz.kinopoisk2.presenter.MyAdapter;
import niaz.kinopoisk2.model.MyMovie;
import niaz.kinopoisk2.model.Tools;

/**
 * Created by NS on 02.11.2016.
 */
public class MoviesActivity extends Activity {
    private ProgressBar         progressBar     = null;
    private int                 cityPosition    = 0;
    private String              cityId          = "";
    private String              cityName        = "";
    private ListView            listViewMovies  = null;                    // ListView
    private ArrayList<MyMovie>  moviesList      = new ArrayList<>();

    /**
     * OnCreate
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);
        cityId = getIntent().getStringExtra("cityID");
        cityName = getIntent().getStringExtra("cityName");
        cityPosition = getIntent().getIntExtra("cityPosition", 0);
        System.out.println("==================Movies=========city=" + cityId);
        initViews();
        downloadMovies();
    }

    /**
     * Process Back key
     */
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MoviesActivity.this, MapsActivity.class);
        intent.putExtra("cityPosition", cityPosition);
        startActivity(intent);
        finish();
    }

    /**
     * Init views
     */
    private void initViews(){
        listViewMovies = (ListView) findViewById(R.id.listViewMovies);
        progressBar = (ProgressBar) findViewById(R.id.movies_progress);
        TextView textViewTitle = (TextView) findViewById(R.id.movies_title);
        textViewTitle.setText(getResources().getString(R.string.movies_in_city) + " " + cityName);

        // Set my adapter
        final MyAdapter myAdapter = new MyAdapter(getApplicationContext(), R.layout.movie, moviesList);
        listViewMovies.setAdapter(myAdapter);

        listViewMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {
                MyMovie myMovie = (MyMovie) adapter.getItemAtPosition(position);
                System.out.println("----------click pos =" + position + " arg3=" + arg3);
                System.out.println("----------click name =" + myMovie.getNameRU());
                Intent intent = new Intent(MoviesActivity.this, MovieActivity.class);
                intent.putExtra("cityID", cityId);
                intent.putExtra("cityName", cityName);
                intent.putExtra("cityPosition", cityPosition);
                intent.putExtra("id", myMovie.getId());
                intent.putExtra("movieName", myMovie.getNameRU());
                startActivity(intent);
                finish();
            }
        });

    }

    /**
     * Process downloaded JSON data and create appropriate classes
     * @param str
     */
    private void setMoviesListData(String str){
        MyMovies myMovies = Tools.convertJsonToMoviesInCity(str);
        if (myMovies == null) {
            System.out.println("-------myMovies == null");
            return;
        }

        ArrayList<MyMovie> filmsData = myMovies.getFilmsData();
        if (filmsData == null) {
            System.out.println("-------filmsData == null");
            return;
        }

        for (MyMovie myMovie : filmsData) {
            moviesList.add(myMovie);
        }

        final MyAdapter myAdapter = new MyAdapter(getApplicationContext(), R.layout.activity_movies, moviesList);
        listViewMovies.setAdapter(myAdapter);
    }

    /**
     * Download movies list in selected city
     */
    private void downloadMovies(){
        AsyncHttpClient client = new AsyncHttpClient();
        String dateString = DateFormat.format("dd.MM.yyyy",
                new Date(System.currentTimeMillis())).toString();
        client.get("http://api.kinopoisk.cf/getTodayFilms?date=" + dateString +
                "&cityID=" + cityId, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                progressBar.setVisibility(View.INVISIBLE);

                System.out.println("---------- movies=" + new String(response));
                setMoviesListData(new String(response));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "Error to download cities", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });    }


}
