package niaz.kinopoisk2.view;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.sql.Date;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import niaz.kinopoisk2.R;
import niaz.kinopoisk2.model.Cinema;
import niaz.kinopoisk2.model.Cinemas;
import niaz.kinopoisk2.model.MyMovie;
import niaz.kinopoisk2.model.MyMovies;
import niaz.kinopoisk2.model.Tools;
import niaz.kinopoisk2.presenter.MyAdapter;

public class CityMapActivity extends FragmentActivity {
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    private String  movieId      = "";
    private String  movieName    = "";
    private String  cityId       = "";
    private String  cityName     = "";
    private int     cityPosition = 0;
    private ArrayList<Cinema> cinemaList = null;

    /**
     * OnCreate
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("============CutyMapActivity===============");

        movieId = getIntent().getStringExtra("id");
        movieName = getIntent().getStringExtra("movieName");
        cityId = getIntent().getStringExtra("cityID");
        cityName = getIntent().getStringExtra("cityName");
        cityPosition = getIntent().getIntExtra("cityPosition", 0);

        System.out.println("------------------cityId=" + cityId);
        setContentView(R.layout.activity_maps);
        downloadCinemas();

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        setUpMapIfNeeded();
//    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * Process Back key
     */
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CityMapActivity.this, MovieActivity.class);
        intent.putExtra("cityID", cityId);
        intent.putExtra("cityName", cityName);
        intent.putExtra("cityPosition", cityPosition);
        intent.putExtra("id", movieId);
        intent.putExtra("movieName", movieName);
        startActivity(intent);
        finish();
    }

    /**
     * Process downloaded JSON data and create appropriate classes
     * @param str
     */
    private void setCinemasData(String str){
        Cinemas cinemas = Tools.convertJsonToCinemas(str);
        if (cinemas != null) {
            cinemaList = cinemas.getCinemaList();
            System.out.println("-----------cinemaList.size=" + cinemaList.size());
            putToMap();
        }
    }

    /**
     * Download movies list in selected city
     */
    private void downloadCinemas(){
        System.out.println("-------------Download cinemas");
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://api.kinopoisk.cf/searchCinemas?cityID=" + cityId, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
//                progressBar.setVisibility(View.INVISIBLE);

                System.out.println("---------- cinemas=" + new String(response));
                setCinemasData(new String(response));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
//                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "-----Error to download cinemas", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }


    /**
     * Put cinemas to map of the city
     */
    private void putToMap(){
        setUpMapIfNeeded();
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        int i = 0;
        for (Cinema cinema : cinemaList) {
            mMap.addMarker(new MarkerOptions().position(
                    new LatLng(Double.parseDouble(cinema.getLat()),
                            Double.parseDouble(cinema.getLon())))
                    .title(cinema.getName())
                    .snippet(cinema.getAddress()));

            // Position using first cinema bacause errors in Kinopoisk database
            if (i++ == 0) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom
                        (new LatLng(Double.parseDouble(cinema.getLat()),
                                Double.parseDouble(cinema.getLon())), 12.0f));
            }
        }
    }


}
