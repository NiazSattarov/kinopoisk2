package niaz.kinopoisk2;

import android.app.Application;
import android.graphics.Bitmap;
import android.test.ApplicationTestCase;

import junit.framework.Assert;

import niaz.kinopoisk2.presenter.MyCache;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
        cacheTest();
    }

    private void cacheTest() {
        System.out.println("-----------Cache Test-----------------");

        // Add record to the cache
        byte[] bytes = {1, 2, 3};
        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = Bitmap.createBitmap(100, 100, conf); // this creates a MUTABLE bitmap
        MyCache.addToCache("test", bitmap);

        // Get record from the cache
        bitmap = MyCache.getFromCache("test");
        Assert.assertNotNull(bitmap);
    }

}